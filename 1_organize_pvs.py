#!/usr/bin/env python3

import argparse
import tqdm
import pandas as pd
from absl import app
from absl.flags import argparse_flags

from sbtvlib import parse_config_file


def parse_args(argv):
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        '--config',
        type=str,
        help='Configuration file with experiments specs.',
        default='configs/vut11.yaml')
    args = parser.parse_args(argv[1:])
    return args


def convert_categorical_to_numeric(category):
    switcher = {
        'muito pior': -3,
        'pior': -2,
        'pouco pior': -1,
        'igual': 0,
        'pouco melhor': 1,
        'melhor': 2,
        'muito melhor': 3
    }
    return switcher.get(category.lower(), None)


def convert_scale_to_numeric(df):
    filter_col = [col for col in df if col.startswith('vc-')]
    for col in tqdm.tqdm(filter_col):
        df[col] = df[col].map(lambda x: convert_categorical_to_numeric(x))
    df = df.reindex(sorted(df.columns), axis=1)
    return df


class Organizer:
    def __init__(self, input, output, sequences):
        self.input = input
        self.output = output
        self.sequences = sequences

    def run(self):
        df = pd.read_csv(self.input)
        df = self.unify_scores_regardless_pseudosequence(df)
        df = convert_scale_to_numeric(df)
        df = df.drop(columns=['SEQ'])
        df.to_csv(self.output, index=False)

    def get_seq1_reordering(self):
        return self.sequences["sequence1"]

    def get_seq2_reordering(self):
        return self.sequences["sequence2"]

    def get_seq3_reordering(self):
        return self.sequences["sequence3"]

    def get_seq_reordering(self, sequence_number):
        switcher = {
            1: self.get_seq1_reordering,
            2: self.get_seq2_reordering,
            3: self.get_seq3_reordering,
        }
        return switcher.get(sequence_number, 0)()

    def unify_scores_regardless_pseudosequence(self, df):
        sequence_ids = sorted(df.SEQ.unique())
        out = []
        for sequence in sequence_ids:
            df_temp = df.loc[df['SEQ'] == sequence]
            rename = self.get_seq_reordering(sequence)
            df_temp = df_temp.rename(columns=rename)
            out.append(df_temp)
        df_out = pd.concat(out)
        return df_out.sort_values(by=['UID'])


def main(args):
    config = parse_config_file(args.config)
    input = config.input_raw_data_csv
    output = config.processed_scores_csv
    sequences = config.sequences
    Organizer(
        input=input,
        output=output,
        sequences=sequences
    ).run()


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)

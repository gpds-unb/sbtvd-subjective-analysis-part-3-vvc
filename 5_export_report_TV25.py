#!/usr/bin/env python3
#
# 4_export_report_as_expected_by_SBTVD_group.py
#
# Author: Pedro Garcia Freitas
# License: MIT
#
# Converts CSV with subjective score table input for the "sureal" software.
#
# The output will be JSON file printed to STDOUT, which may be further used
# for `sureal` input.
import os
import argparse
import regex as re
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
from scipy.optimize import bisect
from scipy.optimize import newton
from tqdm import tqdm
from absl import app
from absl.flags import argparse_flags

from sbtvlib import parse_config_file


# https://www.vqeg.org/VQEGSharedFiles/Publications/Validation_Tests/
# FRTV_Phase2/FRTV_Phase2_Final_Report.pdf
def vqeg_objective_logistic(x, b1, b2, b3):
    """Function based on VQEG's report VQA - PHASEII (page 18)"""
    return b1/(1.0 + np.exp(-b2 * (x-b3)))


def jacobian_of_vqeg_objective_logistic(x, b1, b2, b3):
    """The jacobian of the vqeg_objective_logistic avoids overflow warning."""
    g1 = 1.0/(1.0 + np.exp(-b2*(x - b3)))
    g2 = -b1*(-x + b3)*np.exp(-b2*(x - b3))/(1.0 + np.exp(-b2*(x - b3))) ** 2
    g3 = -b1*b2*np.exp(-b2*(x - b3))/(1.0 + np.exp(-b2*(x - b3))) ** 2
    return np.array([g1, g2, g3]).T


def convert_range(values, original_min, original_max, dest_min, dest_max):
    min_input = original_min
    max_input = original_max
    min_output = dest_min
    max_output = dest_max
    scale_factor = (max_output - min_output) / (max_input - min_input)
    output_values = [((value - min_input) * scale_factor + min_output)
                     for value in values]
    return output_values


def shifted_vqeg_objective_logistic(x, b1, b2, b3, target_value, shift):
    return vqeg_objective_logistic(x, b1, b2, b3) - target_value - shift


def convert_33_to_17(values):
    return convert_range(values, -3, 3, 1, 7)


def convert_17_to_33(values):
    return convert_range(values, 1, 7, -3, 3)


class Reporter:
    def __init__(
            self,
            input_csv,
            output_dir,
            experiment,
            bitrates,
            target_quality
    ):
        self.experiment = experiment
        self.df = pd.read_csv(input_csv)
        self.output_dir = f"{output_dir}_{target_quality}"
        self.bitrates = bitrates
        self.target_quality = target_quality
        os.makedirs(self.output_dir, exist_ok=True)

    def run(self):
        self._process_outlier_removal()
        self._process_bitrate_mos_without_outliers()
        self._process_generate_bitrate_mos_curves_plots()

    def _process_generate_bitrate_mos_curves_plots(self):
        df = self._df_rate_distortion_table.copy()
        iterator = tqdm(df.iterrows(), desc="Processing", leave=True)
        for _, row in iterator:
            video = row["Video"]
            iterator.set_description(f"Processing {video}")
            iterator.refresh()

    def _process_bitrate_mos_without_outliers(self):
        df = self._df_rate_distortion_table.copy()
        output_csv = os.path.join(
            self.output_dir,
            "rate_distortion_videos.csv"
        )
        df.to_csv(
            output_csv,
            index=False
        )

    @property
    def _df_rate_distortion_table(self):
        df = self._df_scores_without_outliers.copy()
        column_names = [col for col in df if col.startswith('vc-')]
        pattern = r"vc-(\w+)_vut(\d+)"
        df_out = []
        for column in column_names:
            v = df[column].values
            match = re.match(pattern, column)
            video = match.group(1)
            bitrate = match.group(2)
            df_out.append({
                "Video": video,
                "Bitrate": bitrate,
                "MOS": np.nanmean(v)
            })
        df_out = pd.DataFrame(df_out)
        df_out = df_out.pivot(index="Video", columns="Bitrate", values="MOS")
        # df_out.loc["Average"] = df_out.mean(numeric_only=True)
        df_out = df_out.reset_index()
        return df_out

    def _process_outlier_removal(self):
        df_no_outliers = self._df_scores_without_outliers
        output_no_outlier_scores = os.path.join(
            self.output_dir,
            "no_outliers_scores.csv"
        )
        df_no_outliers.to_csv(
            output_no_outlier_scores,
            index=False,
            na_rep='NaN'
        )

    @property
    def _df_scores_without_outliers(self):
        df = self.df.copy()
        filter_col = [col for col in df if col.startswith('vc-')]
        for column in filter_col:
            v = df[column].values
            mask = np.abs((v - v.mean(0)) / v.std(0)) > 2
            df[column] = np.where(mask, np.nan, v)
        return df


def parse_args(argv):
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--config',
        type=str,
        help='Configuration file with experiments specs.',
        default='configs/vut12.yaml')
    parser.add_argument(
        '--target_mos_threshold',
        type=int,
        help='Target quality parameter to determine the target bitrate.',
        choices=[-2, -1, 0, 1, 2],
        default=0)
    args = parser.parse_args(argv[1:])
    return args


def main(args):
    target_mos_threshold = args.target_mos_threshold
    args = parse_config_file(args.config)
    Reporter(
        input_csv=args.processed_scores_csv,
        output_dir=args.output_dir,
        experiment=args.experiment,
        bitrates=args.bitrates,
        target_quality=target_mos_threshold
    ).run()


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)

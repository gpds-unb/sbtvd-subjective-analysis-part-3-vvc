#!/usr/bin/env python3
#
# 3_convert_subjective_scores_to_sureal_format_data.py
#
# Author: Pedro Garcia Freitas
# License: MIT
#
# Converts CSV with subjective score table input for the "sureal" software.
#
# The output will be JSON file printed to STDOUT, which may be further used
# for `sureal` input.

import argparse
import json
import pandas as pd
from absl import app
from absl.flags import argparse_flags


class Formatter:
    def __init__(
            self,
            input_csv,
            output_json,
            dataset_name='SBTV-part2',
            yuv_fmt='yuv420p',
            width=1920,
            height=1080,
            ref_score=0.0,
    ):
        self.df = pd.read_csv(input_csv)
        self.output_dataset = output_json
        self.dataset_name = dataset_name
        self.yuv_fmt = yuv_fmt
        self.width = width
        self.height = height
        self.ref_score = ref_score

    def run(self):
        output_data = {
            "dataset_name": self.dataset_name,
            "yuv_fmt": self.yuv_fmt,
            "width": int(self.width),
            "height": int(self.height),
            "ref_score": float(self.ref_score),
            "ref_dir": "",
            "dis_dir": "",
            "ref_videos": [],
            "dis_videos": [],
        }
        ref_videos, ref_id_dict = self._get_ref_ids()
        dis_videos = self._get_pvs_ids(ref_id_dict)
        output_data["ref_videos"] = ref_videos
        output_data["dis_videos"] = dis_videos
        with open(self.output_dataset, "w") as outfile:
            json.dump(output_data, outfile, indent=4)

    def _get_pvs_ids(self, ref_id_dict):
        pvs_ids = []
        counter = 0
        for video_name, score_values in self.df.items():
            if video_name.startswith('vc-'):
                ref_name, pvs_name = video_name.split('_')[0], video_name
                scores = score_values.tolist()
                content_id = ref_id_dict[ref_name]
                line = {
                    'asset_id': counter,
                    'content_id': content_id,
                    'os': scores,
                    'path': f"{pvs_name}.yuv",
                }
                pvs_ids.append(line)
                counter += 1
        return pvs_ids

    def _get_ref_ids(self):
        ref_ids = []
        for video_name in self.df:
            if video_name.startswith('vc-'):
                ref_name = video_name.split('_')[0]
                if ref_name not in ref_ids:
                    ref_ids.append(ref_name)
        ref_id_dict = {value: index for index, value in enumerate(ref_ids)}
        refs = []
        for ref_name, ref_id in ref_id_dict.items():
            refs.append({
                'content_id': ref_id,
                'content_name': ref_name,
                'path': f"{ref_name}.yuv"
            })
        return refs, ref_id_dict


def parse_args(argv):
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--input',
        type=str,
        help='Path for the processed/unscrambled scores (output_scores.csv).',
        default='data/output_scores.csv')
    parser.add_argument(
        '--output',
        type=str,
        help='Path for the dataset formatted to be used in sureal.',
        default='data/output_scores.json'
    )
    args = parser.parse_args(argv[1:])
    return args


def main(args):
    Formatter(
        args.input,
        args.output,
        dataset_name='SBTV-part2',
        yuv_fmt='yuv420p',
        width=1920,
        height=1080,
        ref_score=0.0,
    ).run()


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)

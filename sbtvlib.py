import yaml


__all__ = [ 'parse_config_file' ]


class Struct:
    def __init__(self, **entries): 
        self.__dict__.update(entries)


def parse_config_file(yaml_file):
    with open(yaml_file, 'r') as file:
        config = yaml.safe_load(file)
        args = Struct(**config)
        return args
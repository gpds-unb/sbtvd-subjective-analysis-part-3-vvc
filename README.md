# On the Quality Requirements for the Next Generation of Brazilian Digital Television Broadcasting System



## How to use

This repository contains a set of scripts designed to be executed in a specific sequence. The scripts are named in a way that indicates their intended order of execution. Follow the steps below to effectively use the provided scripts:

```bash
$ python 1_organize_pvs.py --config configs/vut11.yaml
$ python 1_organize_pvs.py --config configs/vut12.yaml
$ python 1_organize_pvs.py --config configs/vut13.yaml
$ python 1_organize_pvs.py --config configs/vut23.yaml
$ python 1_organize_pvs.py --config configs/vut24.yaml
$ python 1_organize_pvs.py --config configs/TV25.yaml
```


```bash
for vut in vut11 vut12 vut13 vut23 vut24;
do
    python 4_export_report_as_expected_by_SBTVD_group.py --config configs/${vut}.yaml --target_mos_threshold -1;
    python 4_export_report_as_expected_by_SBTVD_group.py --config configs/${vut}.yaml --target_mos_threshold 0;
    python 4_export_report_as_expected_by_SBTVD_group.py --config configs/${vut}.yaml --target_mos_threshold 1;
done
```


```bash
$ python 5_export_report_TV25.py --config configs/TV25.yaml --target_mos_threshold 1
```
#!/usr/bin/env python3
#
# 4_export_report_as_expected_by_SBTVD_group.py
#
# Author: Pedro Garcia Freitas
# License: MIT
#
# Converts CSV with subjective score table input for the "sureal" software.
#
# The output will be JSON file printed to STDOUT, which may be further used
# for `sureal` input.
import os
import argparse
import regex as re
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
from scipy.optimize import bisect
from scipy.optimize import newton
from tqdm import tqdm
from absl import app
from absl.flags import argparse_flags
import seaborn as sns

from sbtvlib import parse_config_file


# https://www.vqeg.org/VQEGSharedFiles/Publications/Validation_Tests/
# FRTV_Phase2/FRTV_Phase2_Final_Report.pdf
def vqeg_objective_logistic(x, b1, b2, b3):
    """Function based on VQEG's report VQA - PHASEII (page 18)"""
    return b1/(1.0 + np.exp(-b2 * (x-b3)))


def jacobian_of_vqeg_objective_logistic(x, b1, b2, b3):
    """The jacobian of the vqeg_objective_logistic avoids overflow warning."""
    g1 = 1.0/(1.0 + np.exp(-b2*(x - b3)))
    g2 = -b1*(-x + b3)*np.exp(-b2*(x - b3))/(1.0 + np.exp(-b2*(x - b3))) ** 2
    g3 = -b1*b2*np.exp(-b2*(x - b3))/(1.0 + np.exp(-b2*(x - b3))) ** 2
    return np.array([g1, g2, g3]).T


def convert_range(values, original_min, original_max, dest_min, dest_max):
    min_input = original_min
    max_input = original_max
    min_output = dest_min
    max_output = dest_max
    scale_factor = (max_output - min_output) / (max_input - min_input)
    output_values = [((value - min_input) * scale_factor + min_output)
                     for value in values]
    return output_values


def shifted_vqeg_objective_logistic(x, b1, b2, b3, target_value, shift):
    return vqeg_objective_logistic(x, b1, b2, b3) - target_value - shift


def convert_33_to_17(values):
    return convert_range(values, -3, 3, 1, 7)


def convert_17_to_33(values):
    return convert_range(values, 1, 7, -3, 3)


datasets = ['philips01', 'globo05', 'globo01', 'philips03', 'lcevc01']
markers = ['o', 's', 'D', 'h', '8']
palette = sns.color_palette('tab10', n_colors=len(datasets))
color_dict = dict(zip(datasets, palette))
marker_dict = dict(zip(datasets, markers))


class Reporter:
    def __init__(
            self,
            input_csv,
            output_dir,
            experiment,
            bitrates,
            target_quality
    ):
        self.experiment = experiment
        self.df = pd.read_csv(input_csv)
        self.output_dir = f"{output_dir}_{target_quality}"
        self.bitrates = bitrates
        self.target_quality = target_quality
        os.makedirs(self.output_dir, exist_ok=True)

    def run(self):
        self._process_outlier_removal()
        self._process_bitrate_mos_without_outliers()
        self._process_generate_bitrate_mos_curves_plots()

    def _process_generate_bitrate_mos_curves_plots(self):
        df_mos, df_sos = self._df_rate_distortion_table
        iterator = tqdm(df_mos.iterrows(), desc="Processing", leave=True)
        for i, row in iterator:
            video = row["Video"]
            iterator.set_description(f"Processing {video}")
            iterator.refresh()
            row_std = df_sos.iloc[i]
            self._generate_rd_plot(row, row_std)

    def _generate_rd_plot(self, row, row_std):
        # TODO: bitrates are based on VUT1.2, change it to load from a file
        video_name = row["Video"]
        bitrates = self.bitrates[video_name]
        rename_bitrates = {f"{i+1}": f"{b}" for i, b in enumerate(bitrates)}
        columns_with_scores = ['1', '2', '3', '4', '5']
        x_values = [float(rename_bitrates[c]) for c in columns_with_scores]
        y_values = [float(row[c]) for c in columns_with_scores]
        y_error = [float(row_std[c]) for c in columns_with_scores]
        y_values = convert_33_to_17(y_values)
        popt, _ = curve_fit(
            vqeg_objective_logistic,
            x_values,
            y_values,
            jac=jacobian_of_vqeg_objective_logistic,
            maxfev=2000,
            p0=[4, 0.5, 2]
        )
        b1, b2, b3 = popt
        color = color_dict[video_name]
        marker=marker_dict[video_name]
        plt.figure()
        plt.scatter(x_values, convert_17_to_33(y_values))
        min_x, max_x = min(x_values) - 1, max(x_values) + 1.5
        x_line = np.linspace(min_x, max_x, 201)
        y_line = vqeg_objective_logistic(x_line, b1, b2, b3)
        y_line = convert_17_to_33(y_line)
        plt.plot(x_line, y_line, '-', color=color)
        plt.errorbar(
            x_values,
            convert_17_to_33(y_values),
            y_error,
            linestyle='None',
            marker=marker,
            markersize=8,
            capsize=3,
            color=color
        )
        fig_path = os.path.join(
            self.output_dir,
            f"{video_name}.pdf"
        )
        plt.axhline(y=1, color='yellowgreen', linestyle='dotted')
        plt.axhline(y=-1, color='red', linestyle='dotted')
        plt.axhline(y=0, color='olive', linestyle='dotted')
        try:
            x_crosses = bisect(
                shifted_vqeg_objective_logistic,
                min_x, max_x,
                args=(b1, b2, b3, 4, self.target_quality)
            )
        except ValueError:
            try:
                x_crosses = newton(
                    shifted_vqeg_objective_logistic,
                    max_x + 500,
                    maxiter=5000,
                    tol=1E-10,
                    args=(b1, b2, b3, 4, self.target_quality)
                )
            except RuntimeError:
                s = "It is not possible to determine the target bitrate of"
                print(f"{s} {video_name}")
        if 'x_crosses' in locals():
            plt.axvline(
                x=x_crosses,
                color=color,
                linestyle='--'
            )
            plt.text(
                x_crosses + 0.2, -3,
                f"Minimal bitrate={x_crosses:.2f}",
                ha='left', va='bottom',
                color=color
            )
        plt.title(f"{self.experiment} - {video_name}")
        plt.xticks(x_values)
        plt.yticks(np.arange(-3, 4))
        plt.ylabel("MOS grade")
        plt.xlabel("Bitrate (Mbps)")
        plt.savefig(fig_path)

    def _process_bitrate_mos_without_outliers(self):
        df, _ = self._df_rate_distortion_table
        output_csv = os.path.join(
            self.output_dir,
            "rate_distortion_videos.csv"
        )
        df.to_csv(
            output_csv,
            index=False
        )

    @property
    def _df_rate_distortion_table(self):
        df = self._df_scores_without_outliers
        column_names = [col for col in df if col.startswith('vc-')]
        pattern = r"vc-(\w+)_rate(\d+)"
        df_out = []
        for column in column_names:
            v = df[column].values
            match = re.match(pattern, column)
            video = match.group(1)
            bitrate = match.group(2)
            df_out.append({
                "Video": video,
                "Bitrate": bitrate,
                "MOS": np.nanmean(v),
                "SOS": np.nanstd(v)
            })
        df_out = pd.DataFrame(df_out)
        df_out_mos = df_out.pivot(index="Video", columns="Bitrate", values="MOS")
        df_out_sos = df_out.pivot(index="Video", columns="Bitrate", values="SOS")
        # df_out.loc["Average"] = df_out.mean(numeric_only=True)
        df_out_mos = df_out_mos.reset_index()
        df_out_sos = df_out_sos.reset_index()
        return df_out_mos, df_out_sos

    def _process_outlier_removal(self):
        df_no_outliers = self._df_scores_without_outliers
        output_no_outlier_scores = os.path.join(
            self.output_dir,
            "no_outliers_scores.csv"
        )
        df_no_outliers.to_csv(
            output_no_outlier_scores,
            index=False,
            na_rep='NaN'
        )

    @property
    def _df_scores_without_outliers(self):
        df = self.df.copy()
        filter_col = [col for col in df if col.startswith('vc-')]
        for column in filter_col:
            v = df[column].values
            mask = np.abs((v - v.mean(0)) / v.std(0)) > 2
            df[column] = np.where(mask, np.nan, v)
        return df


def parse_args(argv):
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--config',
        type=str,
        help='Configuration file with experiments specs.',
        default='configs/vut12.yaml')
    parser.add_argument(
        '--target_mos_threshold',
        type=int,
        help='Target quality parameter to determine the target bitrate.',
        choices=[-2, -1, 0, 1, 2],
        default=0)
    args = parser.parse_args(argv[1:])
    return args


def main(args):
    target_mos_threshold = args.target_mos_threshold
    args = parse_config_file(args.config)
    Reporter(
        input_csv=args.processed_scores_csv,
        output_dir=args.output_dir,
        experiment=args.experiment,
        bitrates=args.bitrates,
        target_quality=target_mos_threshold
    ).run()


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)
